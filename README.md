# <img src="public/icons/icon_48.png" width="45" align="left"> Jira Transfer

Chrome Extension used to register your work log into jira tasks

## Features

- Keep a link to your tasks
- Register work logs into that tasks

## Install

[**Chrome** extension](https://chrome.google.com/webstore/detail/jira-worklog/elbmoegcdmjigdefmmpjhhhdjleefcpe?hl=es&authuser=0) 

## Contribution

Suggestions and pull requests are welcomed!.

---

This project was bootstrapped with [Chrome Extension CLI](https://github.com/dutiyesh/chrome-extension-cli)

