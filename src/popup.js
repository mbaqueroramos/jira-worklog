"use strict";
import "./popup.css";
import {
  currentDay,
  daysBetweenDates,
  addDaysToDate,
  isWorkDay,
  timeOffsetStr,
} from "./dateUtils.js";
import {
  stripJiraUrl,
  sendJiraEventBetweenDates,
  loadJiraTitle,
  fetchJiraCurrentUser,
  fetchJiraWorklogsForDate,
} from "./jiraUtils.js";
import { clearAllNotifications } from "./notificationUtils.js";
import { findSiblingInput, toggleToDateVisibility, hasClass, removeClass, addClass, isVisible, setupListener, removeChildren } from './domUtils.js';

(function () {
  // We will make use of Storage API to get and store `initialConfig` value
  // More information on Storage API can we found at
  // https://developer.chrome.com/extensions/storage

  // To get storage access, we have to mention it in `permissions` property of manifest.json file
  // More information on Permissions can we found at
  // https://developer.chrome.com/extensions/declare_permissions

  const jiraWorklogStorage = {
    get: (cb) => {
      chrome.storage.sync.get(["extensionValues"], (result) => {
        cb(result.extensionValues || {});
      });
    },
    set: (value, cb = () => {}) => {
      chrome.storage.sync.set(
        {
          extensionValues: value,
        },
        cb
      );
    },
  };

  const jiraWorklogBackup = {
    get: (cb) => {
      chrome.storage.sync.get(["jiraWorklogBackup"], (result) => {
        cb(result.jiraWorklogBackup || {});
      });
    },
    set: (value, cb = () => {}) => {
      chrome.storage.sync.set(
        {
          jiraWorklogBackup: value,
        },
        cb
      );
    },
  };

  let currentUser = null;

  document.addEventListener("DOMContentLoaded", initValuesFromStorage);

  function initValuesFromStorage() {
    jiraWorklogStorage.get((initialConfig) => {
      if (typeof initialConfig === "undefined") {
        jiraWorklogStorage.set(initialConfig, () => {
          setupInputs(initialConfig);
        });
      } else {
        setupInputs(initialConfig);
      }
    });
    setupListeners();
  }

  function setupInputs(initialValue = {}) {
    const today = currentDay();
    document.getElementById("fromDay").value = today;
    document.getElementById("toDay").value = today;
    const allItems = initialValue["all-items"];
    if (allItems) {
      for (const itemSaved of allItems) {
        const domItem = document.getElementById(itemSaved.id);

        if (domItem) {
          console.log("TODO ---> set values");
        } else {
          addItem(itemSaved);
        }
      }
    }

    setJiraUrl(initialValue.jiraUrl || "https://jira-url.es");
    document.getElementById("ajiraUrl").innerHTML = getJiraUrl();
    reloadJiraUrlItems();
    if (initialValue.jiraUrl) {
      loadCurrentUser(true).then(loadLoggedHoursForInputDates);
    }
  }

  function resetInputs() {
    setJiraUrl("");
    removeChildren(document.getElementById("all-items"));
  }
  function setJiraUrl(value) {
    document.getElementById("jiraUrl").value = value;
  }
  function getJiraUrl() {
    let jiraUrlValue = document.getElementById("jiraUrl").value;
    jiraUrlValue = stripJiraUrl(jiraUrlValue);
    document.getElementById("jiraUrl").value = jiraUrlValue;
    return jiraUrlValue;
  }

  function setupListeners() {
    setupListener("jiraUrl", "change", saveJiraUrl);
    setupListener("jiraUrl", "focusout", editJiraUrl);
    setupListener("ajiraUrl", "click", openJiraUrl);
    setupListener("ajiraUrl", "auxclick", openJiraUrl);
    setupListener("addItem", "click", addItemAndSave);
    setupListener("editJiraUrl", "click", editJiraUrl);
    setupListener("toggleJiraItems", "click", toggleJiraItems);
    Array.prototype.slice
      .call(document.getElementsByClassName("toggleToDate"))
      .forEach((elem) => {
        elem.addEventListener("click", toggleToDateVisibility);
      });
    Array.prototype.slice
      .call(document.getElementsByClassName("previousDay"))
      .forEach((elem) => {
        elem.addEventListener("click", previousDay);
      });
    Array.prototype.slice
      .call(document.getElementsByClassName("nextDay"))
      .forEach((elem) => {
        elem.addEventListener("click", nextDay);
      });

    setupListener("fromDay", "change", loadLoggedHoursForInputDates);
    setupListener("toDay", "change", loadLoggedHoursForInputDates);
    setupListener("openTransferBtn", "click", openTransfer);
  }

  function saveJiraUrl(inputEvent) {
    const newJiraValue = inputEvent.target.value;
    saveItem({ jiraUrl: newJiraValue }, reloadJiraUrlItems);
  }

  function reloadJiraUrlItems() {
    Array.prototype.slice
      .call(document.getElementsByTagName("input"))
      .forEach((inputItem) => {
        if (/Task$/.test(inputItem.id)) {
          inputItem.dispatchEvent(new FocusEvent("focusout"));
        }
      });
  }

  function editJiraUrl() {
    let inputType = document.getElementById("jiraUrl").type;
    let displayAnchor = "";
    if (inputType === "text") {
      inputType = "hidden";
    } else {
      inputType = "text";
      displayAnchor = "none";
    }
    document.getElementById("jiraUrl").type = inputType;
    document.getElementById("ajiraUrl").innerHTML = getJiraUrl();
    document.getElementById("ajiraUrl").style.display = displayAnchor;
  }

  function openJiraUrl(evt) {
    window.open(document.getElementById("ajiraUrl").innerHTML);
    try {
      evt.preventDefault();
      evt.stopPropagation();
    } finally {
      // ok
    }
  }

  function toggleJiraItems() {
    jiraWorklogBackup.get((backupValues) => {
      jiraWorklogStorage.get((extensionValues) => {
        jiraWorklogBackup.set(extensionValues, () => {
          jiraWorklogStorage.set(backupValues, () => {
            resetInputs();
            setupInputs(backupValues);
          });
        });
      });
    });
  }

  

  function previousDay(evt) {
    const inputElem = findSiblingInput(evt.target);
    let currDayStr = inputElem.value;
    currDayStr = addDaysToDate(currDayStr, -1);
    inputElem.value = currDayStr;
    inputElem.dispatchEvent(new Event("change"));
    clearAllNotifications();
    return currDayStr;
  }

  function nextDay(evt) {
    const inputElem = findSiblingInput(evt.target);
    let currDayStr = inputElem.value;
    currDayStr = addDaysToDate(currDayStr, 1);
    inputElem.value = currDayStr;
    inputElem.dispatchEvent(new Event("change"));
    clearAllNotifications();
    return currDayStr;
  }

  function createInputs({
    id = null,
    task = "",
    text = "",
    start = "8:00",
    time = "1h",
  }) {
    const itemName = id || "item" + Date.now().toString(24);
    const div = document.createElement("div");
    const sendButton = document.createElement("button");
    div.className = "jira-item card";
    div.id = itemName;
    sendButton.id = "send-" + itemName;
    sendButton.className = "button";
    sendButton.innerHTML = "Send";

    const infoIcon = document.createElement("i");
    infoIcon.className = "icon-i";
    infoIcon.id = "notification-" + itemName;
    infoIcon.style.display = "none";

    div.innerHTML = `
      <div class="top-btnbar">
        <button class="icon-btn">🗑</button>
        <span class="time-spent"><span>
      </div>
      <div class="card-content">
      ${createTitle(itemName, task)}
      ${createInput(itemName, "Task", "Task", task)}
      ${createInput(itemName, "Text", "Comment", text)}
      ${createInput(itemName, "Start", "Time", start)}
      ${createInput(itemName, "Time", "Duration", time)}
      </div>`;

    div.appendChild(sendButton);
    div.appendChild(infoIcon);

    sendButton.addEventListener("click", clickSendToJira);
    div
      .getElementsByClassName("icon-btn")[0]
      .addEventListener("click", removeItem);

    Array.prototype.slice
      .call(div.getElementsByTagName("input"))
      .find((input) => input.id === itemName + "Task")
      .addEventListener("focusout", sanitizeJiraId);

    Array.prototype.slice
      .call(div.getElementsByTagName("input"))
      .forEach((inputItem) => {
        inputItem.addEventListener("focusout", () => {
          saveJiraItem(getTaskItems(getTaskPrefix(itemName)));
        });
        if (inputItem.id === itemName + "Task") {
          if (task !== "" && getJiraUrl()) {
            loadJiraTitle(
              getJiraUrl(),
              task,
              itemName,
              document.getElementById(itemName + "Title")
            );
          }
          inputItem.addEventListener("focusout", (evt) => {
            loadJiraTitle(
              getJiraUrl(),
              evt.target.value,
              itemName,
              document.getElementById(itemName + "Title")
            );
          });
        }
      });

    Array.prototype.slice
      .call(div.getElementsByTagName("a"))
      .forEach((anchorItem) => {
        anchorItem.addEventListener("click", (evt) => {
          window.open(evt.target.href);
        });
      });
    return div;
  }

  function createTitle(itemName, task) {
    return `<div>
    <a id="${itemName}Title" class='task-title' href='#'>${task}</a>
    </div>`;
  }

  function createInput(itemName, inputName, labelText, value = "") {
    return `<div>
    <label class="task-label" for="${itemName}${inputName}">${labelText}</label>
    <input id="${itemName}${inputName}" type="text" value="${value}"/>
    </div>`;
  }

  function addItemAndSave() {
    const values = {
      id: "item" + Date.now().toString(24),
      task: "",
      text: "",
      start: "8:00",
      time: "1h",
    };
    const itemAdded = addItem(values);
    saveJiraItem(values);
    return itemAdded;
  }

  function addItem(values) {
    const allItems = document.getElementById("all-items");
    const itemAdded = createInputs(values);
    allItems.append(itemAdded);
    return values;
  }

  function removeItem(pointerEvent, cb = () => {}) {
    if (hasClass(pointerEvent.target, "loading")) {
      return;
    }
    addClass(pointerEvent.target, "loading");
    const cardItem = pointerEvent.target.parentElement.parentElement;
    jiraWorklogStorage.get((extensionValues) => {
      const extensionAllItems = extensionValues["all-items"] || [];
      extensionAllItems.splice(
        extensionAllItems.findIndex(
          (savedItem) => savedItem.id === cardItem.id
        ),
        1
      );
      extensionValues["all-items"] = extensionAllItems;
      jiraWorklogStorage.set(extensionValues, cb);
    });
    removeFadeOut(cardItem, 500);
  }

  function removeFadeOut(el, speed) {
    const seconds = speed / 1000;
    el.style.transition = "opacity " + seconds + "s ease";
    el.style.opacity = 0;
    setTimeout(function () {
      el.parentNode.removeChild(el);
    }, speed);
  }

  function sanitizeJiraId(pointerEvent, cb = () => {}) {
    let value = pointerEvent.target.value;
    value = value.trim();
    value = value.replace(/\/+$/, "");
    value = value.replace(/.*\/+/, "");
    pointerEvent.target.value = value;
  }

  function clickSendToJira(pointerEvent) {
    if (hasClass(pointerEvent.target, "loading")) {
      return;
    }
    addClass(pointerEvent.target, "loading");
    sendJiraEventBetweenDates(
      getJiraUrl(),
      getFromDayToDay(),
      getTaskItems(getTaskPrefix(pointerEvent.target.id))
    )
      .then(loadLoggedHoursForInputDates)
      .finally(() => {
        removeClass(pointerEvent.target, "loading");
      });
  }


  function getTaskPrefix(buttonId) {
    return buttonId.replace("send-", "");
  }
  
  function getTaskItems(prefix) {
    return {
      id: prefix,
      task: document.getElementById(prefix + "Task").value,
      start: document.getElementById(prefix + "Start").value,
      time: document.getElementById(prefix + "Time").value,
      text: document.getElementById(prefix + "Text").value,
    };
  }

  function saveJiraItem(item, cb = () => {}) {
    jiraWorklogStorage.get((extensionValues) => {
      const allItems = extensionValues["all-items"] || [];
      const itemIndex = allItems.findIndex(
        (savedItem) => savedItem.id === item.id
      );
      if (itemIndex > -1) {
        allItems[itemIndex] = item;
      } else {
        allItems.push(item);
      }
      extensionValues["all-items"] = allItems;
      jiraWorklogStorage.set(extensionValues, cb);
    });
  }

  function saveItem(item, cb = () => {}) {
    jiraWorklogStorage.get((extensionValues) => {
      let newExtensionValues = { ...extensionValues, ...item };

      jiraWorklogStorage.set(newExtensionValues, cb);
    });
  }

  function getFromDayToDay() {
    const fromDayElem = document.getElementById("fromDay");
    const toDayElem = document.getElementById("toDay");
    const fromDay = fromDayElem.value;
    const toDay = isVisible(toDayElem) ? toDayElem.value : fromDay;
    return { fromDay, toDay };
  }

  function openTransfer() {
    window.open(chrome.runtime.getURL("index.html"), "_blank");
  }

  let loadingLoggedHours = false;
  function loadLoggedHoursForInputDates() {
    if (loadingLoggedHours) {
      return;
    }
    loadingLoggedHours = true;
    document.getElementById("timeSpentToday").innerHTML = "Loading...";
    document.getElementById("timeSpentToday").title = "";
    loadCurrentUser().then(() => {
      const { fromDay, toDay } = getFromDayToDay();
      const endDate = addDaysToDate(toDay, 1);
      loadJiraWorklogsForDate(fromDay, endDate)
        .then((numHours) => {
          document.getElementById(
            "timeSpentToday"
          ).innerHTML = `Logged hours (${numHours})`;
          document.getElementById("timeSpentToday").title = currentUser.name;
        })
        .catch(() => {
          document.getElementById("timeSpentToday").innerHTML = "Not connected";
        });
    });
    loadingLoggedHours = false;
  }

  function loadCurrentUser(force = false) {
    if (!currentUser || force) {
      return new Promise((resolve) => {
        fetchJiraCurrentUser(getJiraUrl()).then((json) => {
          currentUser = json;
          resolve(currentUser);
          document.getElementsByClassName("title")[0].title = currentUser.name;
          chrome.runtime.sendMessage("REMINDER", (response) => {
            console.log("received user data", response);
          });
        });
      });
    }
    return Promise.resolve(currentUser);
  }

  function loadJiraWorklogsForDate(startDate, endDate) {
    if (!currentUser) {
      return;
    }
    return fetchJiraWorklogsForDate(
      getJiraUrl(),
      currentUser,
      startDate,
      endDate
    );
  }
})();
