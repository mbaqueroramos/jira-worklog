"use strict";
import "./index.css";
import {
  firstDayOfMonthStr,
  lastDayOfMonthStr,
  fourWeeksDayStr,
  yyyyMMddStr,
  workdaysBetweenDates,
  timeOffsetStr,
} from "./dateUtils.js";
import { removeClass, addClass, setupListener, removeChildren } from "./domUtils.js";
import { addIssuesToTable, clearTable, showTableSpinner, hideTableSpinner, showErrorInTable, hideErrorInTable } from "./transferTable.js";
import {
  stripJiraUrl,
  fetchAllJiraWorklogsDataForDate,
  fetchAllJiraWorklogsDataForDateAndOwner,
  groupWorklogsByIssue,
  fetchJiraCurrentUser,
  fetchJiraDataOpenPetitions,
  fetchSubtasks,
  fetchTasksAndSubtasks,
  editMeta,
  isSameUser,
  fetchAllWorklogsForIssue
} from "./jiraUtils.js";
import { processIssues } from "./jiraTransfer.js";

(function () {
  document.addEventListener("DOMContentLoaded", initApp);

  let valuesFromStorage;
  let leftWorklogs = [];
  let rightWorklogs = [];
  let ownerPetitions = [];
  let fullProcess = [];
  let tableValues = {};
  let adminMode = false;
  let nonImputableTasks = [];
  let vacations = {};
  let leftUser;
  let rightUser;

  function initApp() {
    (async function () {
      valuesFromStorage = await loadValuesFromStorage();
      setupInputs(valuesFromStorage);
      setupListeners();
    })();
  }



  async function setupInputs(valuesFromStorage) {
    const jiraWorklogIndex = valuesFromStorage.jiraWorklogIndex;
    document.getElementById("fromDay").value = fourWeeksDayStr();
    document.getElementById("toDay").value = lastDayOfMonthStr();

    for (const inputId in jiraWorklogIndex) {
      const htmlElem = document.getElementById(inputId);
      if (htmlElem?.value !== undefined) {
        htmlElem.value = jiraWorklogIndex[inputId];
      }
    }
    
    loadUrlsFromStorage(valuesFromStorage);
    
  }

  function loadUrlsFromStorage(valuesFromStorage){
    setJiraUrlValue("jiraUrlFrom", valuesFromStorage.extensionValues.jiraUrl);
    setJiraUrlValue("jiraUrlTo", valuesFromStorage.jiraWorklogBackup.jiraUrl);

    refreshWorklogs();
  }

  function setupListeners() {
    setupListener("projectName", "change", saveInputEvent);
    setupListener("owner", "change", saveInputEvent);
    setupListener("owner", "focusout", checkAdminMode);
    setupListener("workers", "change", changeWorkers);
    setupListener("workers", "focusout", recalculateNonImputable);
    setupListener("nonImputable", "change", saveInputEvent);
    setupListener("nonImputable", "focusout", recalculateNonImputable);
    setupListener("toggleJiraItems", "click", toggleJiraItems);
    setupListener("addAction", "click", addNewOptionComboActions);
    setupListener("dropAction", "click", dropOptionComboActions);
    setupListener("btnTransferTasks", "click", runTransferTasks);
    setupListener("btnTransferAll", "click", runTransferAll);
    setupListener("btnRefreshWorklogs", "click", refreshWorklogs);
    setupListener("btnOpenEstimate", "click", openEstimate);
    setupListener("editJiraUrls", "click", editJiraUrls);
    setupListener("eJiraUrlFrom", "change", saveJiraUrls);
    setupListener("eJiraUrlTo", "change", saveJiraUrls);
    setupListener("eJiraUrlFrom", "focusout", restoreJiraUrls);
    setupListener("eJiraUrlTo", "focusout", restoreJiraUrls);
    setupListener("openPetitions", "click", setDefaultTargetIssue);
  }

  function getJiraUrlFrom() {
    return document.getElementById("jiraUrlFrom").href;
  }

  function getJiraUrlTo() {
    return document.getElementById("jiraUrlTo").href;
  }

  function checkAdminMode() {
    adminMode = rightUser && isSameUser({ key: valuesFromStorage?.jiraWorklogIndex?.owner}, rightUser);
    if (adminMode) {
      removeClass(document.getElementById("adminModeContainer"), "display-none");
    } else {
      addClass(document.getElementById("adminModeContainer"), "display-none");
    }
    return adminMode;
  }

  function editJiraUrls() {
    const hrefJiraUrlFrom = document.getElementById("jiraUrlFrom").href;
    const hrefJiraUrlTo = document.getElementById("jiraUrlTo").href;
    addClass(document.getElementById("jiraUrlFrom"), "display-none");
    addClass(document.getElementById("jiraUrlTo"), "display-none");
    document.getElementById("eJiraUrlFrom").type = "text";
    document.getElementById("eJiraUrlTo").type = "text";
    document.getElementById("eJiraUrlFrom").value = hrefJiraUrlFrom;
    document.getElementById("eJiraUrlTo").value = hrefJiraUrlTo;
  }

  function saveJiraUrls() {
    const jiraUrlFromValue = document.getElementById("eJiraUrlFrom").value;
    const jiraUrlToValue = document.getElementById("eJiraUrlTo").value;

    valuesFromStorage.extensionValues.jiraUrl = stripJiraUrl(jiraUrlFromValue);
    valuesFromStorage.jiraWorklogBackup.jiraUrl = stripJiraUrl(jiraUrlToValue);
    chrome.storage.sync.set(valuesFromStorage);

  }

  function restoreJiraUrls() {
    const hrefJiraUrlFrom = document.getElementById("eJiraUrlFrom").value;
    const hrefJiraUrlTo = document.getElementById("eJiraUrlTo").value;
    removeClass(document.getElementById("jiraUrlFrom"), "display-none");
    removeClass(document.getElementById("jiraUrlTo"), "display-none");
    document.getElementById("eJiraUrlFrom").type = "hidden";
    document.getElementById("eJiraUrlTo").type = "hidden";
    document.getElementById("jiraUrlFrom").href = hrefJiraUrlFrom;
    document.getElementById("jiraUrlFrom").innerHTML = hrefJiraUrlFrom;
    document.getElementById("jiraUrlTo").href = hrefJiraUrlTo;
    document.getElementById("jiraUrlTo").innerHTML = hrefJiraUrlTo;
  }

  async function setDefaultTargetIssue(event) {
    const selectedOption = event.target;
    
    tableValues.defaultTask = selectedOption.value;

    if (adminMode) {
      fillInputsWithSelectedTask(event);
      recalculateNonImputable();
    }
    saveProperty('defaultTask', tableValues.defaultTask);

    await loadSubtasks();
      
    reloadTable();
  }

  function fillInputsWithSelectedTask(item) {
    const selectedOption = item.target || document.querySelector(`option[value=${item.defaultTask}]`);

    const saleEstimate = selectedOption.getAttribute('saleEstimate');
    const internalEstimate = selectedOption.getAttribute('internalEstimate');
    const expectedStartDate = selectedOption.getAttribute('expectedStartDate');
    const expectedDeliveryDate = selectedOption.getAttribute('expectedDeliveryDate');
    document.getElementById('saleEstimate').value = saleEstimate;
    document.getElementById('internalEstimate').value = internalEstimate;
    document.getElementById('expectedStartDate').value = yyyyMMddStr(expectedStartDate);
    document.getElementById('expectedDeliveryDate').value = yyyyMMddStr(expectedDeliveryDate);
  }

  async function loadSubtasks() {
    const subtaskResult = await fetchSubtasks(tableValues.jiraUrlTo, tableValues.defaultTask);
    
    tableValues.subTasks = subtaskResult.issues;
    refreshMappingsSelector();
  }

  function refreshMappingsSelector() {
    const mappingsSelector = document.querySelector("#mappingsSelector");
    removeChildren(mappingsSelector);
    for (const issue of tableValues.subTasks) {
      const newOption = document.createElement("option");
      newOption.innerHTML = '[' + normalizeSubTaskTitle(issue.fields.summary) + '] => ' + issue.key + ' ' + issue.fields.summary;
      mappingsSelector.appendChild(newOption);
    }
    for (const issue of nonImputableTasks) {
      const newOption = document.createElement("option");
      newOption.innerHTML = '[' + normalizeSubTaskTitle(issue.fields.summary) + '] => ' + issue.key + ' ' + issue.fields.summary;
      mappingsSelector.appendChild(newOption);
    }
  }

  function normalizeSubTaskTitle(summary){
    const summaryText = summary.replace(/.*\//,'');
    const nomalizedText = summaryText.normalize('NFKD').replace(/[^\w\s]/g, '');
    return nomalizedText.trim();
  }

  function getProjectName() {
    return document.getElementById("projectName").value;
  }

  function getOwner() {
    return document.getElementById("owner").value;
  }

  function refreshWorklogs() {
    const usersLoaded = [];
    const worklogsloaded = [];
    const jiraUrlFrom = getJiraUrlFrom();
    const jiraUrlTo = getJiraUrlTo();
    const projectName = getProjectName();
    const startDate = document.getElementById("fromDay").value;
    const endDate = document.getElementById("toDay").value;
    const owner = getOwner();
    tableValues = { ...tableValues, jiraUrlFrom, jiraUrlTo, projectName};
    
    clearTable();
    showTableSpinner();

    const jiraUrlToPromise = fetchJiraUserAndShowError(jiraUrlTo, 'jiraUrlTo');
    const jiraUrlFromPromise = fetchJiraUserAndShowError(jiraUrlFrom, 'jiraUrlFrom');
    usersLoaded.push(jiraUrlFromPromise);
    usersLoaded.push(jiraUrlToPromise);
    jiraUrlToPromise.then(()=> {
      comboActionsDataLoad(jiraUrlTo);
      loadOpenPetitionsByOwner(jiraUrlTo, owner);
    });

    const p = new Promise((resolve) => {
      Promise.all(usersLoaded).then(([currentUserFrom, currentUserTo]) => {
        leftUser = currentUserFrom;
        rightUser = currentUserTo;
        
        worklogsloaded.push(loadJiraIssues(jiraUrlFrom, currentUserFrom, startDate, endDate).then(issues => {
          leftWorklogs = issues.issues;
        }).catch(() => {
          leftWorklogs = [];
          resolve(false);
        }));
        
        worklogsloaded.push(loadJiraIssuesWithOwner(jiraUrlTo, currentUserTo, owner, startDate, endDate).then(issues => {
          rightWorklogs = issues.issues;
        }).catch(() => {
          rightWorklogs = [];
          resolve(false);
        }));
        checkAdminMode();
        Promise.all(worklogsloaded).then(() => {
          resolve(true);
        }).catch(() => {
          resolve(false);
        });
      }).catch(() => {
        resolve(false);
      });
    });
    
    p.then((finishedOk) => {
      hideTableSpinner();
      if (!finishedOk) {
        showErrorInTable();
        return;
      }
      hideErrorInTable();
      fullProcess = processIssues(leftWorklogs, rightWorklogs);
      tableValues = { ...tableValues, fullProcess, ownerPetitions, leftUser, rightUser};
      addIssuesToTable(tableValues);
    });
  }

  function fetchJiraUserAndShowError(jiraUrl, inputName) {
    const promiseConnecting = fetchJiraCurrentUser(jiraUrl);
    promiseConnecting.then((result) => {
      console.log('fetchJiraUserAndShowError then ', jiraUrl, inputName, result);
      if (!result.name) {
        throw new Error(result);
      }
      hideErrorConnectingJira(inputName);
    }).catch((result) => {
      console.log('fetchJiraUserAndShowError catch ', jiraUrl, inputName, result);
      showErrorConnectingJira(inputName);
    });
    return promiseConnecting;
  }

  function reloadTable() {
    
    clearTable();
    addIssuesToTable(tableValues);
  }

  function showErrorConnectingJira(jiraId) {
    removeClass(document.getElementById("notification-" + jiraId), "hidden");
  }
  function hideErrorConnectingJira(jiraId) {
    addClass(document.getElementById("notification-" + jiraId), "hidden");
  }


  function loadValuesFromStorage() {
    return new Promise(resolve => {

      chrome.storage.sync.get(["jiraWorklogIndex", "extensionValues", "jiraWorklogBackup", "jwProjectName", "jwOwner", "jwDefaultTask", "jwProjectActions"], (result) => {
        if (!result.jiraWorklogIndex) {
          result.jiraWorklogIndex = {
            projectName: result.jwProjectName,
            owner: result.jwOwner,
            defaultTask: result.jwDefaultTask,
            projectActions: result.jwProjectActions
          };
          chrome.storage.sync.set({ "jiraWorklogIndex": result.jiraWorklogIndex });
        }
        resolve(result);
      });
    })
  }

  function changeWorkers(changeEvent) {
    const workersInput = changeEvent.target;
    const workersCleanValue = workersInput.value.replace(/^\W*/, '').replace(/\W*$/, '').replace(/[,\s]+/,',');
    saveProperty(workersInput.id, workersCleanValue);
    const arrWorkers = workersCleanValue.split(',');
    if (arrWorkers.length > 0) {
      workersInput.labels[0].innerHTML = "Workers: (" + arrWorkers.length + ")";
    } else {
      workersInput.labels[0].innerHTML = "Workers:";
    }
  }
  
  async function recalculateNonImputable() {
    const jiraUrl = valuesFromStorage.jiraWorklogBackup.jiraUrl;
    vacations = {};
    const workers = valuesFromStorage.jiraWorklogIndex.workers.split(',');
    if (!workers?.length) {
      return;
    }
    for (const worker of workers) {
      vacations[worker] = [];
    }
    vacations._total = 0;
    const nonImputableTasksResult = await fetchTasksAndSubtasks({jiraUrl, keylist: valuesFromStorage.jiraWorklogIndex.nonImputable });
    nonImputableTasks = nonImputableTasksResult?.issues || [];
    refreshMappingsSelector();
    const promises = [];
    for (const issue of nonImputableTasks) {
      promises.push(fetchAllWorklogsForIssue(jiraUrl, issue));
    }
    const expectedStartDate = document.getElementById('expectedStartDate').value;
    const expectedDeliveryDate = document.getElementById('expectedDeliveryDate').value;
    const limitDateStart = new Date(expectedStartDate);
    const limitDateEnd = new Date(expectedDeliveryDate);
    Promise.all(promises).then(issueWorklogs => {
      for (const worklogs of issueWorklogs) {
        for (let workLog of worklogs) {
          let workLogDate = new Date(workLog.started);
          if (
            limitDateStart <= workLogDate &&
            limitDateEnd >= workLogDate
          ) {
            console.log('this worklog is in date: ', workLogDate, workLog.author.key);
            for (const worker of workers) {
              if (workLog.author.name === worker || workLog.author.key === worker) {
                vacations[worker].push(workLogDate);
                ++vacations._total;
              }
            }
          }
        }
      }
      let vacationDaysDescription = '';
      for (const worker of workers) {
        vacationDaysDescription += worker + ': ' + vacations[worker].length + '\n';
      }
      document.getElementById("vacationDays").value = vacations._total;
      document.getElementById("vacationDays").title = vacationDaysDescription;

      const numWorkDays = workdaysBetweenDates(expectedDeliveryDate, expectedStartDate);
      document.getElementById('evolutiveDays').value = numWorkDays;
      document.getElementById('workDays').value = (numWorkDays * workers.length) - vacations._total;
      document.getElementById('workDays').title = `(${numWorkDays} * ${workers.length}) - ${vacations._total}`;
    });
  }

  function saveInputEvent(changeEvent) {
    saveInputValue(changeEvent.target);
  }
  function saveInputValue(input) {
    saveProperty(input.id, input.value);
  }
  
  function saveProperty(propertyName, value) {
    valuesFromStorage.jiraWorklogIndex[propertyName] = value;
    chrome.storage.sync.set({"jiraWorklogIndex": valuesFromStorage.jiraWorklogIndex });
  }
  
  function toggleJiraItems() {
    const extensionValues = valuesFromStorage.jiraWorklogBackup;
    const jiraWorklogBackup = valuesFromStorage.extensionValues;
    valuesFromStorage.extensionValues = extensionValues;
    valuesFromStorage.jiraWorklogBackup = jiraWorklogBackup;
    chrome.storage.sync.set(valuesFromStorage);
    loadUrlsFromStorage(valuesFromStorage);
  }

  function setJiraUrlValue(inputId, jiraUrl) {
    const htmlElem = document.getElementById(inputId);
    if (htmlElem) {
      const storageItemValue = stripJiraUrl(jiraUrl || "");
      htmlElem.href = storageItemValue;
      htmlElem.innerHTML = storageItemValue;
    }
  }

  async function loadJiraIssues(jiraUrl, currentUser, startDate, endDate) {
    
    const worklogs = await fetchAllJiraWorklogsDataForDate(jiraUrl, currentUser, startDate, endDate);
        
    const issues = groupWorklogsByIssue(worklogs);
    issues.sort(compareByKey);
    return { issues, currentUser };
  }

  async function loadJiraIssuesWithOwner(jiraUrl, currentUser, owner, startDate, endDate) {
    
    const worklogs = await fetchAllJiraWorklogsDataForDateAndOwner(jiraUrl, currentUser, owner, startDate, endDate);
        
    const issues = groupWorklogsByIssue(worklogs);
    issues.sort(compareByKey);
    return { issues, currentUser };
  }

  function compareByKey(a, b) {
    if (a.key > b.key) {
      return 1;
    }
    if (a.key < b.key) {
      return -1;
    }
    return 0;
  }

  function runTransferAll() {
    runTransferTasks();
    runTransferWorklogs();
    if (isAnyLoadingLeft() || isAnyWorklogLeft()) {
      setTimeout(() => runTransferAll(), 500);
    }
  }

  function runTransferTasks() {
    console.log('runTransferTasks');
    const taskBtnIterator = document.evaluate('//*[@id="processPanelBody"]/tr/td/button[contains(text(),"Create Task")]', document, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
    return clickAllNodes(taskBtnIterator);
  }
  function runTransferWorklogs() {
    console.log('runTransferWorklogs');
    const taskBtnIterator = document.evaluate('//*[@id="processPanelBody"]/tr/td/button[contains(text(),"Transfer WL")]', document, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
    return clickAllNodes(taskBtnIterator);
  }
  function clickAllNodes(xpathNodes) {
    let mutableNode;
    const allbuttons = [];
    while (mutableNode = xpathNodes.iterateNext()) {
      allbuttons.push(mutableNode);
    }
    for (mutableNode of allbuttons) {
      mutableNode.click();
    }
    return allbuttons.length > 0;
  }
  function openEstimate() {

    const issueIdOrKey = document.getElementById('openPetitions').value;
    window.open(`${valuesFromStorage.jiraWorklogBackup.jiraUrl}/browse/${issueIdOrKey}`);
    
  }

  function isAnyWorklogLeft() {
    const xpath = document.evaluate('//*[@id="processPanelBody"]/tr/td[contains(text(),"TASK FIRST")]', document, null, XPathResult.ANY_UNORDERED_NODE_TYPE, null);
    return xpath.singleNodeValue !== null;
  }
  function isAnyLoadingLeft() {
    const xpath = document.evaluate('//*[@id="processPanelBody"]/tr/td[contains(text(),"Loading...")]', document, null, XPathResult.ANY_UNORDERED_NODE_TYPE, null);
    return xpath.singleNodeValue !== null;
  }

  function comboActionsDataLoad(jiraUrl) {
    chrome.storage.sync.get(["jwProjectActions"], (result) => {
      //console.log('Stored:' + result.jwProjectActions)

      if (result.jwProjectActions) {
        for (const newValue of result.jwProjectActions) {
          addOptionComboActions(newValue);
        }
      }
    });
  }

  function loadOpenPetitionsByOwner(jiraUrl, owner) {
    
    const project = getProjectName();
    if (!owner || !project) {
      return;
    }
    const openPetitionsElement = document.getElementById("openPetitions");
    removeChildren(openPetitionsElement);
    const p = fetchJiraDataOpenPetitions(jiraUrl, owner, project);
    p.then((json) => {
      ownerPetitions = json;
      if (json?.errorMessages?.length) {
        return;
      }
      for (const issue of json.issues) {
        addOptionOpenPetitions(openPetitionsElement, issue);
      }
      if (!tableValues.defaultTask) {
        tableValues.defaultTask = valuesFromStorage.jiraWorklogIndex.defaultTask;
      }
      openPetitionsElement.value = tableValues.defaultTask;
      if (!openPetitionsElement.value && openPetitionsElement.firstElementChild) {
        tableValues.defaultTask = openPetitionsElement.firstElementChild.value;
        openPetitionsElement.value = tableValues.defaultTask;
      }
      loadSubtasks();
      if (adminMode) {
        fillInputsWithSelectedTask(tableValues);
        recalculateNonImputable();
      }
    });
    return p;
  }

  function addOptionComboActions(newValue) {
    const newOption = document.createElement("option");
    newOption.innerHTML = newValue || "";
    document.getElementById("mappingsSelector").appendChild(newOption);
  }

  function addNewOptionComboActions() {
    const newValue = prompt("Ingrese una acción: ");
    addOptionComboActions(newValue);

    chrome.storage.sync.get(["jwProjectActions"], (result) => {
      const arr = result.jwProjectActions || [];
      arr.push(newValue);

      chrome.storage.sync.set({ jwProjectActions: arr }).then(() => {
        console.log("Value is set to " + arr);
      });
    });
  }

  function dropOptionComboActions() {
    const $select = document.querySelector("#mappingsSelector");
    const index = $select.selectedIndex;
    const optionSelected = $select.options[index];

    optionSelected.remove();

    chrome.storage.sync.get(["jwProjectActions"], (result) => {
      const arr = result.jwProjectActions;
      arr.splice(optionSelected, 1);

      chrome.storage.sync.set({ jwProjectActions: arr }).then(() => {
        console.log("Value is set to " + arr);
      });
    });
  }

  function addOptionOpenPetitions(openPetitionsElement, issue) {
    const newOption = document.createElement("option");
    newOption.innerHTML = issue.fields.summary;
    newOption.value = issue.key;
    newOption.aggregatetimeoriginalestimate=issue.aggregatetimeoriginalestimate;
    /*
  customfield_22631 // Sale estimate
  customfield_22634 // Internal estimate
  customfield_23215 // Expected start date
  customfield_23213 // Expected delivery date
  */
    newOption.setAttribute('saleEstimate', issue.fields.customfield_22631 || '');
    newOption.setAttribute('internalEstimate', issue.fields.customfield_22634 || '');
    newOption.setAttribute('expectedStartDate', issue.fields.customfield_23215 || '');
    newOption.setAttribute('expectedDeliveryDate', issue.fields.customfield_23213 || '');
    openPetitionsElement.appendChild(newOption);
  }

})();
