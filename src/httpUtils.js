
export function get(url) {
  return httpRest(url);
}

export function post(url, body) {
    return httpRest(url, "POST", body);
}

export function put(url, body) {
  return httpRest(url, "PUT", body);
}

export function httpRest(url, method = "GET", body = null) {
    return new Promise((resolve, reject) => {
      fetch(url, {
        headers: {
          accept: "application/json, text/javascript, */*; q=0.01",
          "content-type": "application/json",
        },
        method,
        credentials: "include",
        body
      }).then(async (a) => {
        let jsonResponse = null;
        let responseOk = false;
        if (a.status >= 200 && a.status < 400) {
          responseOk = true;
        }
        try {
          jsonResponse = await a.json();
          responseOk = true;
        } catch (err) {
          console.error('httpRest unexpected', url, err);
        }
        if (responseOk) {
          resolve(jsonResponse);
        } else {
          reject({ url, err });
        }
      }).catch(err => {
        console.info('httpRest fetch', url, err);
        reject({ url, err });
      });
    });
  }

