
export function notifySuccess(item, text = "") {
    notify(item, "&#x2713;", text);
  }
export function notifyFailures(item, messages = []) {
    let text = "";
    for (const message of messages) {
      text += message + "\n";
    }
    notifyFailure(item, text);
  }
export function notifyFailure(item, text = "") {
    notify(item, "&#x26A0;", text);
  }
export function notifyClear(item) {
    notify(item);
  }

export function clearAllNotifications() {
    const allIcons = document.getElementsByClassName("icon-i");
    for (const infoIcon of allIcons) {
      if (/^notification-/.test(infoIcon.id)) {
        notifyClear({ id: infoIcon.id.replace(/^notification-/, "") });
      }
    }
  }

export function notify(item, entity = "", text = "") {
    const notificationItem = document.getElementById("notification-" + item.id);
    notificationItem.innerHTML = entity;
    notificationItem.title = text;
    if (!entity) {
      notificationItem.style.display = "none";
      return;
    }
    notificationItem.style.display = "";
  }