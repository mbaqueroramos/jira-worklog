import { removeClass, addClass, stringifySafe, removeChildren } from "./domUtils.js";
import { shortenDateStr } from "./dateUtils.js";
import {sendJiraWorklog, createTask} from './jiraUtils.js';

export const TASK_FIRST = 'TASK FIRST';
export const NEW_WL = 'NEW WL';
export const WL_CREATED = 'CREATED';
export const TASK_CREATED = 'TASK_CREATED';
export const NEW_TASK = 'NEW TASK';

export function clearTable() {
    removeChildren(document.getElementById('processPanelBody'));
}

export function showTableSpinner() {
    addClass(document.getElementById('tableSpinner'), 'show');
}

export function hideTableSpinner() {
    removeClass(document.getElementById('tableSpinner'), 'show');
}

export function showErrorInTable() {
    addClass(document.getElementById('errorInTable'), 'show');
}

export function hideErrorInTable() {
    removeClass(document.getElementById('errorInTable'), 'show');
}

export function addIssuesToTable(mainPageValues) {
    const panel = document.getElementById('processPanelBody');
    for (const issue of mainPageValues.fullProcess) {
        addIssueToTable({ ...mainPageValues, issue, panel });
    }
}

function composeSummary(issue) {
    let summary = issue.fields.summary;
    if (issue.fields.parent) {
        return composeSummary(issue.fields.parent) + ' (' + summary + ')';
    }
    return summary;
}
function addIssueToTable(tableValues) {

    const issue = tableValues.issue;
    let worklogs = issue.proccessedWorklogs || issue.worklogs;
    worklogs = worklogs.sort(compareByStarted);
    const taskValues = {
        ...tableValues,
        key: issue.key,
        summary: composeSummary(issue),
        version: issue.version,
        equivalentIssue: issue.equivalentIssue,
        action: inferEquivalentAction(issue),
        item: issue
    };
    addRowWithValues(taskValues);

    for (const worklog of worklogs) {
        if (worklog.started) {
            const worklogValues = {
                ...tableValues,
                timeSpent: worklog.timeSpent,
                comment: worklog.comment,
                started: worklog.started,
                equivalentIssue: issue.equivalentIssue,
                action: inferEquivalentAction(issue, worklog),
                item: worklog
            };
            addRowWithValues(worklogValues);
        }
    }
}


function inferEquivalentAction(issue, worklog) {
    if (worklog) {
        if (worklog.equivalentWorklog) {
            return WL_CREATED;
        }
        if (issue.equivalentIssue) {
            return NEW_WL;
        }
        return TASK_FIRST;
    }
    if (issue.equivalentIssue?.fields?.summary) {
        return TASK_CREATED;
    }
    return NEW_TASK;
}

export function compareByStarted(a, b) {
    if (a.started < b.started) {
        return 1;
    }
    if (a.started > b.started) {
        return -1;
    }
    return 0;
}


function addRowWithValues(values) {
    const { key, summary, version, timeSpent, comment, started, equivalentIssue, action, panel, item, jiraUrlFrom, jiraUrlTo } = values;
    const tr = document.createElement("tr");
    panel.appendChild(tr);
    addCellToTable(tr, linkToKey(key, jiraUrlFrom));
    if (summary) {
        const tdSummary = addCellToTable(tr, summary);
        tdSummary.colSpan = 3;
        tdSummary.className = "align-left";
        addCellToTable(tr, version).className = "align-right";
    } else {
        const tdTimespent = addCellToTable(tr, timeSpent);
        tdTimespent.className = "align-right";
        tdTimespent.colSpan = 2;
        addCellToTable(tr, comment).className = "align-left";
        const tdStarted = addCellToTable(tr, shortenDateStr(started));
        tdStarted.className = "align-left";
    }

    let firstReceiverCell;
    if (action === NEW_TASK) {
        firstReceiverCell = addTransferTaskBtnToTable({ tr, task: item, ...values });
    } else if (action === TASK_CREATED) {
        firstReceiverCell = addCellToTable(tr, linkToKey(equivalentIssue?.key, jiraUrlTo) + ' ' + equivalentIssue?.fields?.summary);
        addClass(firstReceiverCell, "align-left");
    } else if (action === NEW_WL) {
        firstReceiverCell = addTransferWorklogBtnToTable({ tr, worklog: item, equivalentIssue, jiraUrlTo });
    } else {
        firstReceiverCell = addCellToTable(tr, action);
        tr.setAttribute("data-worklog", stringifySafe({
            comment: item.comment,
            started: item.started,
            timeSpentSeconds: item.timeSpentSeconds
        }));
        addClass(firstReceiverCell, "align-left");
    }
    if (firstReceiverCell) {
        addClass(firstReceiverCell, "border-l");
    }
}

function linkToKey(key, jiraUrl) {
    if (key) {
        return `<a href='${jiraUrl}/browse/${key}'>[${key}]</a>`;
    }
    return key;
}

function addCellToTable(tr, value) {
    const td = document.createElement("td");
    td.innerHTML = value || "";
    tr.appendChild(td);
    return td;
}

function addTransferTaskBtnToTable(taskItems) {
    const {tr, task, ownerPetitions, jiraUrlTo, projectName, rightUser, defaultTask} = taskItems;
    const destinationTask = findDestinationTask(task, ownerPetitions, defaultTask);
    
    return addTransferBtnToTable({
        tr,
        value: 'Create Task in [' + destinationTask.fields.summary + ' ]',
        action: async () => {
            let issueType = '10606'; // sub-construction
            if (task?.fields?.issuetype?.id === '10004') {
                issueType = '15417'; // sub-defect
            }
            const taskCreated = await createTask(jiraUrlTo, {
                projectName: projectName,
                petitionId: destinationTask.key,
                assigneeName: rightUser.name,
                summary: '[' + task.key + '] ' + task.fields.summary,
                description: task.description || task.fields.summary,
                issueType
            });
            return taskCreated;
        }, actionThen: (jiraCreated, td, _btn) => {
            td.innerHTML = `<a href="${jiraUrlTo}/browse/${jiraCreated.key}">[${jiraCreated.key}]</a> ${jiraCreated.fields.summary}`;
            addClass(td, 'align-left');
            activateWorklogItems(taskItems, jiraCreated);
        }, actionCatch: (_result, td, btn) => {
            td.innerHTML = '';
            td.appendChild(btn);
            removeClass(btn, 'align-left');
        }
    });
}

function activateWorklogItems(taskItems, jiraCreated) {
    let currTr = taskItems.tr;
    while (currTr.nextSibling) {
        currTr = currTr.nextSibling;
        const allTDs = Array.from(currTr.querySelectorAll('td'));
        const transferWlTd = allTDs[allTDs.length -1];
        if (transferWlTd.innerHTML === TASK_FIRST) {
            const worklog = JSON.parse(currTr.getAttribute("data-worklog"));
            transferWlTd.innerHTML='';
            addTransferWorklogBtnToTable({ tr: currTr, td: transferWlTd, worklog, equivalentIssue: jiraCreated, jiraUrlTo });
        } else {
            break;
        }
    }
}

function findDestinationTask(task, ownerPetitions, defaultTask) {
    const taskVersion = task.version;
    let latestTask;
    for (const ownerPetition of ownerPetitions.issues) {
        if (compareDigits(ownerPetition.fields.summary, taskVersion)) {
            return ownerPetition;
        }
        if (defaultTask === ownerPetition.key) {
            latestTask = ownerPetition;
        }
        if (!latestTask || latestTask.created < ownerPetition.created) {
            latestTask = ownerPetition;
        }
    }
    return latestTask;
}

function compareDigits(field1, field2) {
    const onlydigitsExpr = /[^\d-]*/g;
    return field1 && field2 && field1.replace(onlydigitsExpr, '').includes(field2.replace(onlydigitsExpr, ''))
}

function addTransferWorklogBtnToTable(worklogItems) {
    const { tr, td, worklog, equivalentIssue, jiraUrlTo } = worklogItems;
    return addTransferBtnToTable({
            tr,
            td,
            value: 'Transfer WL',
            action: async () => {
                await sendJiraWorklog(jiraUrlTo, {
                    key: equivalentIssue.key,
                    comment: worklog.comment,
                    datetime: worklog.started,
                    timeSpentSeconds: worklog.timeSpentSeconds
                });
                return WL_CREATED;
            },
            actionThen: (result, td, _btn) => {
                td.innerHTML = result;
                addClass(td, 'align-left');
            },
            actionCatch: (_result, td, btn) => {
                td.innerHTML = '';
                td.appendChild(btn);
                removeClass(btn, 'align-left');
            }
        });
}

function addTransferBtnToTable({ tr, td, value, action, actionThen, actionCatch }) {
    const btn = document.createElement("button");
    td = td || document.createElement("td");
    btn.innerHTML = value || "";
    btn.className = "button";
    btn.addEventListener('click', () => {
        td.innerHTML = 'Loading...';
        action().then((result) => {
            actionThen?.(result, td, btn);
        }).catch((result) => {
            actionCatch?.(result, td, btn);
        });
    });
    td.appendChild(btn);
    tr.appendChild(td);
    return td;
}