import {
    currentDay,
    firstDayOfMonthDate,
    isWorkDay,
    lastDayOfMonthDate
  } from "./dateUtils.js";

export const SUNDAY = 0;
export const MONDAY = 1;
export const TUESDAY = 2;
export const WEDNESDAY = 3;
export const THURSDAY = 4;
export const FRIDAY = 5;
export const SATURDAY = 6;
export const NUM_DAYS = 7;

export const DayOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

export function createCalendar(htmlElement, withDate = new Date(), startOnDay = MONDAY) {
  creteDaysOfWeek(htmlElement, startOnDay);
  creteDaysOfMonth(htmlElement, withDate, startOnDay);
}

function creteDaysOfWeek(htmlElement, startOnDay = MONDAY) {
    const div = document.createElement("div");
    div.className = 'days-of-week';
    htmlElement.appendChild(div);
    for (let i = 0; i < NUM_DAYS; ++i) {
        div.innerHTML += createDayOfWeek((i + startOnDay) % NUM_DAYS);
    }
}

function createDayOfWeek(numDay) {
    return `<div class="weekDay">${DayOfWeek[numDay]}</div>`
}

function creteDaysOfMonth(htmlElement, withDate, startOnDay = MONDAY) {
    const divForMonth = document.createElement("div");
    divForMonth.className = 'days-of-month';
    htmlElement.appendChild(divForMonth);
    let firstDayOfMonth = firstDayOfMonthDate(withDate);
    let lastDayOfMonth = lastDayOfMonthDate(withDate);
    const monthStartsInDay = firstDayOfMonth.getDay() - startOnDay;
    const maxDays = lastDayOfMonth.getDate();
    let currDayPrinting = 0;
    {
        let divForWeek = document.createElement("div");
        divForWeek.className = 'week';
        divForMonth.appendChild(divForWeek);
        for (let i = 0; i < monthStartsInDay; ++i) {
            divForWeek.appendChild(createDayOfMonth(' - '));
        }
        for (let i = monthStartsInDay; i < NUM_DAYS; ++i) {
            divForWeek.appendChild(createDayOfMonth(++currDayPrinting));
        }
    }
    while (currDayPrinting < maxDays) {
        let divForWeek = document.createElement("div");
        divForWeek.className = 'week';
        divForMonth.appendChild(divForWeek);
        let i = 0;
        for (; i < NUM_DAYS && currDayPrinting < maxDays; ++i) {
            divForWeek.appendChild(createDayOfMonth(++currDayPrinting));
        }
        if (currDayPrinting >= maxDays) {
            for (; i < NUM_DAYS; ++i) {
                divForWeek.appendChild(createDayOfMonth(' - '));
            }
        }
    }
}

function createDayOfMonth(numDay) {
    const div = document.createElement('div');
    div.className = 'weekDay';
    div.innerHTML = numDay;
    div.addEventListener('drop', (ev) => {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        ev.target.appendChild(document.getElementById(data));
    });
    div.addEventListener('dragover', (evt) => {
        evt.preventDefault();
    });
    if (numDay > 0) {
        for (let i=0; i<3; ++i) {
            div.appendChild(createTaskForDay("FRONTAC"+i, "Descriptoin", "time"));
        }
    }
    return div;
}

function createTaskForDay(taskId, taskDescription, taskDuration) {
    const div = document.createElement('div');
    div.className = 'worklog';
    div.draggable = "true";
    div.id = taskId + "rand" + (+new Date());
    div.addEventListener('dragstart', (ev) => {
        ev.dataTransfer.setData("text", ev.target.id);
        ev.stopPropagation();
    });
    
    div.innerHTML = `<span>${taskId}</span><span>${taskDescription}</span><span>${taskDuration}</span>`;
    return div;
}