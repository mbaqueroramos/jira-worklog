"use strict";
import {
  addDaysToDate,
  convertToValidVariable,
  isWorkDay,
  yyyyMMdd,
} from "./dateUtils.js";

import { get } from "./httpUtils.js";

chrome.runtime.onMessage.addListener((request) => {
  if (request === "REMINDER") {
    
    chrome.storage.sync.get(["extensionValues"], (result) => {
      const jiraUrl = result.extensionValues?.jiraUrl;
      if (jiraUrl) {
        queryUser(jiraUrl).then((userName) => {
          console.log('Jira Worklog extension logged with', userName);
          const params = generateParams(userName, jiraUrl);
          queryWeekWorklogs(params).then((worklogForDates) => {
            remindMissingDays(params, worklogForDates);
          });
        });
      }
    });
  }
});

function generateParams(userName, jiraUrl) {
  const currDate = new Date();
  const firstDayIndex = 0;
  const yesterdayIndex = -1;
  const lastDayIndex = -7;
  const currDateStr = yyyyMMdd(currDate);
  const startDate = addDaysToDate(currDateStr, lastDayIndex);
  const endDate = addDaysToDate(currDateStr, yesterdayIndex);
  const fromDate = new Date(startDate);
  const toDate = new Date(endDate);

  const params = {
    userName,
    jiraUrl,
    currDate,
    currDateStr,
    firstDayIndex,
    lastDayIndex,
    fromDate,
    toDate,
    startDate,
    endDate,
  };
  return params;
}

function queryUser(jiraUrl) {
  const queryJiraUser = jiraUrl + "/rest/auth/latest/session";
  return new Promise((resolve) => {
    get(queryJiraUser).then((result) => {
      resolve(result.name);
    }).catch((err) => {
      console.warn('Jira Worklog Extension: User not logged in to jira', err);
    });
  });
}

function queryWeekWorklogs(params) {
  const { jiraUrl, startDate, currDateStr } = params;

  const queryLastWeekWorklogs =
    jiraUrl +
    "/rest/api/latest/search?jql=worklogAuthor%20in%20(currentUser())%20and%20worklogDate%20%3E%3D%20%27" +
    startDate +
    "%27%20and%20worklogDate%20%3C%20%27" +
    currDateStr +
    "%27&fields=summary%2Cworklog%2Cissuetype%2Cparent%2Cproject%2Cstatus%2Ctimeoriginalestimate%2Ctimeestimate%2Cversions%2Ccustomfield_14405&maxResults=1000";
  if (process.env.NODE_ENV !== "production") {
    console.log("queryLastWeekWorklogs", queryLastWeekWorklogs);
  }
  return new Promise((resolve) => {
    get(queryLastWeekWorklogs).then((result) => {
      const issues = result?.issues;
      const worklogForDates = {};
      const allIssues = [];
      issues?.forEach((issue) => {
        const p = getAllWorklogsForIssue(issue, jiraUrl);
        allIssues.push(
          new Promise((resolve) => {
            p.then((worklogs) => {
              for (const worklog of worklogs) {
                addWorklogForDates(worklogForDates, worklog, params);
              }
            });
            resolve(worklogForDates);
          })
        );
      });
      Promise.all(allIssues).then(() => {
        resolve(worklogForDates);
      });
    });
  });
}

function getAllWorklogsForIssue(issue, jiraUrl) {
  return new Promise((resolve) => {
    if (issue.fields.worklog.maxResults < issue.fields.worklog.total) {
      get(jiraUrl + "/rest/api/latest/issue/" + issue.key + "/worklog").then(
        (workLogBig) => {
          resolve(workLogBig.worklogs);
        }
      );
    } else {
      resolve(issue.fields.worklog.worklogs);
    }
  });
}

function addWorklogForDates(
  worklogForDates,
  worklog,
  { fromDate, currDate, userName }
) {
  const worklogDate = new Date(worklog.started);
  if (
    fromDate <= worklogDate &&
    currDate >= worklogDate &&
    worklog.author.key === userName
  ) {
    const worklogDateStr = worklog.started.split("T")[0];
    let timeSpentSeconds = worklogForDates[worklogDateStr];
    if (!timeSpentSeconds) {
      timeSpentSeconds = 0;
    }
    timeSpentSeconds += worklog.timeSpentSeconds;
    worklogForDates[worklogDateStr] = timeSpentSeconds;
    worklogForDates[convertToValidVariable(worklogDateStr)] = timeSpentSeconds;
  }
}

function remindMissingDays(params, worklogForDates) {
  const { firstDayIndex, lastDayIndex, fromDate, endDate } = params;
  
  let day = null;
  const missing = [];
  for (let i = firstDayIndex; i > lastDayIndex; --i) {
    day = addDaysToDate(endDate, i);
    const strDay = convertToValidVariable(day);
    if (isWorkDay(day)) {
      
      if (
        !worklogForDates[strDay] ||
        worklogForDates[strDay] === 0 ||
        (worklogForDates[strDay] > 1000 && worklogForDates[strDay] < 25000)
      ) {
        missing.push(day);
      }
    }
  }

  notifyMissingDaysIfNeeded(missing);
}

function notifyMissingDaysIfNeeded(missing) {

  if (missing.length > 0) {
    chrome.storage.sync.get(["missingDays"], (result) => {
      let missingDays = result.missingDays;
      if (!missingDays) {
        missingDays = [];
      }
      let userNotified = true;
      for (const missingDay of missing) {
        if (!missingDays.includes(missingDay)) {
          userNotified = false;
        }
      }
      if (!userNotified) {
        chrome.notifications.create({
          type: "basic",
          iconUrl: "icons/icon_48.png",
          title: "Work reminder",
          message:
            "Please, check those days with pending working hours:\n" +
            missing.join("\n"),
          priority: 0,
        });
      }

      chrome.storage.sync.set({ missingDays: missing }, () => {});
    });
  }
}
