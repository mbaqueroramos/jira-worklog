"use strict";

import {
  addDaysToDate,
  daysBetweenDates,
  isWorkDay,
  timeOffsetStr,
} from "./dateUtils.js";
import { get, post, put } from "./httpUtils.js";

import {
  notifyClear,
  notifyFailure,
  notifyFailures,
  notifySuccess,
} from "./notificationUtils.js";

export function stripJiraUrl(jiraUrlValue) {
  jiraUrlValue = jiraUrlValue.replace(/\/browse\/.*/, "");
  jiraUrlValue = jiraUrlValue.replace(/\/issues\/.*/, "");
  jiraUrlValue = jiraUrlValue.replace(/\/+$/, "");
  return jiraUrlValue;
}

export function sendJiraEventBetweenDates(
  jiraUrl,
  { fromDay, toDay },
  currentItem
) {
  const fromDate = new Date(fromDay);
  const toDate = new Date(toDay);
  const numDays = daysBetweenDates(fromDate, toDate) + 1;
  let day = null;
  const p = [];
  for (let i = 0; i < numDays; ++i) {
    day = addDaysToDate(fromDay, i);
    if (isWorkDay(day) || numDays === 1) {
      p.push(sendJiraEvent(jiraUrl, currentItem, day));
    }
  }
  return Promise.all(p);
}

export function sendJiraEvent(jiraUrl, currentItem, day) {
  const currTimeOffset = timeOffsetStr();
  const body =
    '{"comment":"' +
    currentItem.text +
    '","started":"' +
    day +
    "T" +
    currentItem.start +
    ":00.000" +
    currTimeOffset +
    '","timeSpent":"' +
    currentItem.time +
    '"}';
  return post(
    jiraUrl +
      "/rest/api/latest/issue/" +
      currentItem.task +
      "/worklog?adjustEstimate=AUTO",
    body
  )
    .then((json) => {
      console.log(json);
      notifySuccess(currentItem, "Work registered ok");
    })
    .catch((_err) => {
      notifyFailure(
        currentItem,
        "Please, check url login, vpn, parameters..\n" + body
      );
    });
}

export async function sendJiraWorklog(jiraUrl, {key, comment, datetime, timeSpentSeconds}) {
  const body = 
    '{"comment":"' +
    comment +
    '","started":"' +
    datetime +
    '","timeSpentSeconds":"' +
    timeSpentSeconds +
    '"}';
  return post(
    jiraUrl +
      "/rest/api/latest/issue/" + key + "/worklog?adjustEstimate=NEW&newEstimate=0h",
    body
  );
}

export async function createTask(jiraUrl, {projectName, petitionId, assigneeName, summary, description, issueType, originalEstimate}) {

  const bodyFields = {
      fields: {
          project: {
              key: projectName,
          },
          parent: {
              key: petitionId,
          },
          assignee: {
              name: assigneeName,
          },
          summary: summary,
          description: description,
          issuetype: {
              id: issueType,
          }
      }
  };
  if (originalEstimate) {
    bodyFields.fields.timetracking = {originalEstimate};
  }

  const body = JSON.stringify(bodyFields);
  let issueCreated = await post(
    jiraUrl + "/rest/api/latest/issue/",
    body
  );
  
  issueCreated = await get(issueCreated.self);
  try {
    await post(
      jiraUrl + "/rest/api/latest/issue/" + issueCreated.key + "/transitions",
      '{"transition":{"id":"11"}}'
    );
    issueCreated.transition = true;
  } finally {
    // ok
  }
  return issueCreated;
}

export function editMeta(jiraUrl, issueIdOrKey, fields) {
  const body = JSON.stringify({ fields });
  return put(`${jiraUrl}/rest/api/2/issue/${issueIdOrKey}/editmeta`,
    body
  );
}

export function loadJiraTitle(jiraUrl, task, id, jiraAnchor) {
  const apiUrl = jiraUrl + "/rest/api/latest/issue/" + task;
  const browseUrl = jiraUrl + "/browse/" + task;
  jiraAnchor.href = browseUrl;
  get(apiUrl)
    .then((json) => {
      if (json.errorMessages && json.errorMessages.length) {
        notifyFailures({ id }, json.errorMessages);
      } else {
        jiraAnchor.innerHTML = json.fields.summary;
        jiraAnchor.title =
          json.fields.summary + " [" + json.fields.status.name + "]";
      }
      notifyClear({ id });
    })
    .catch(() => {
      notifyFailure(
        { id },
        'Please, check url "' + jiraUrl + '"\nlogin, vpn, parameters..'
      );
    });
}

export function fetchJiraCurrentUser(jiraUrl) {
  const apiUrl = jiraUrl + "/rest/auth/latest/session";
  return get(apiUrl);
}

export function fetchJiraWorklogsSearch(jiraUrl, startDate, endDate) {
  const apiUrl =
    jiraUrl +
    "/rest/api/latest/search?jql=(worklogAuthor%20in%20(currentUser())%20and%20worklogDate%20%3E%3D%20%27" +
    startDate +
    "%27%20and%20worklogDate%20%3C%3D%20%27" +
    endDate +
    "%27)or(assignee%20in%20(currentUser())%20and%20created%20>" +
    startDate +
    ")&fields=summary%2Cworklog%2Cissuetype%2Cparent%2Cproject%2Cstatus%2Ctimeoriginalestimate%2Ctimeestimate%2Cversions%2Ccustomfield_14405%2Ccustomfield_15820&maxResults=100000";
  return get(apiUrl);
}

export function fetchJiraWorklogsToDestiny(jiraUrl, owner, startDate, endDate) {
  const apiUrl = jiraUrl +
    "/rest/api/latest/search?jql=%28%28worklogAuthor%20%3D%20currentUser%28%29%20or%20reporter%20%3D%20currentUser%28%29%29%20and%20worklogDate%20%3E%3D%20%27"
    + startDate + "%27%20and%20worklogDate%20%3C%3D%20%27"
    + endDate + "%27%20%29%20or%20%28issueFunction%20in%20subtasksOf%28%22reporter%20%3D%20%27"
    + owner + "%27%20and%20type%20in%20%28Evolutive%2C%20Evolutivo%29%20%20and%20status%20%21%3D%20Delivered%22%29%29"
    + "&fields=summary%2Cworklog%2Cissuetype%2Cparent%2Cproject%2Cstatus%2Ctimeoriginalestimate%2Ctimeestimate%2Cversions&maxResults=100000";
  return get(apiUrl);
}


export function fetchJiraWorklogsForDate(
  jiraUrl,
  currentUser,
  startDate,
  endDate
) {
  return new Promise((resolveWorklogs, reject) => {
    fetchJiraWorklogsSearch(jiraUrl, startDate, endDate).then((result) => {
      const issues = result.issues;
      const limitDateStart = new Date(startDate);
      const limitDateEnd = new Date(endDate);
      const allIssues = [];
      if (!issues) {
        reject(0);
        return;
      }
      sumHoursInJiraIssues(
        jiraUrl,
        currentUser,
        issues,
        allIssues,
        limitDateStart,
        limitDateEnd
      ).then((totalHours) => {
        resolveWorklogs(totalHours);
      });
    });
  });
}

export function sumHoursInJiraIssues(
  jiraUrl,
  currentUser,
  issues,
  allIssues,
  limitDateStart,
  limitDateEnd
) {
  let totalHours = 0;
  issues.forEach((issue) => {
    let p = fetchAllWorklogsForIssue(jiraUrl, issue);
    allIssues.push(p);
    allIssues.push(
      new Promise((resolveSumHours) =>
        p.then((worklogs) => {
          let issueTimeInSeconds = 0;
          for (let workLog of worklogs) {
            let workLogDate = new Date(workLog.started);
            if (
              limitDateStart <= workLogDate &&
              limitDateEnd >= workLogDate &&
              isSameUser(workLog.author, currentUser)
            ) {
              issueTimeInSeconds += workLog.timeSpentSeconds;
            }
          }
          totalHours += issueTimeInSeconds / 3600;
          resolveSumHours(totalHours);
        })
      )
    );
  });
  return new Promise((resolve) => {
    Promise.all(allIssues).then(() => {
      resolve(totalHours);
    });
  });
}

export function isSameUser(user1, user2) {
  return (user1.key && (user1.key === user2.name ||
  user1.key === user2.key)) ||
  (user1.name && (user1.name === user2.name ||
  user1.name === user2.key));
}

export function fetchAllWorklogsForIssue(jiraUrl, issue) {
  return new Promise((resolve) => {
    if (issue.fields.worklog.maxResults < issue.fields.worklog.total) {
      get(jiraUrl + "/rest/api/latest/issue/" + issue.key + "/worklog").then(
        (workLogBig) => {
          resolve(workLogBig.worklogs);
        }
      );
    } else {
      resolve(issue.fields.worklog.worklogs);
    }
  });
}

export function fetchAllJiraWorklogsDataForDate(
  jiraUrl,
  currentUser,
  startDate,
  endDate
) {
  return new Promise((resolveWorklogs, reject) => {
    fetchJiraWorklogsSearch(jiraUrl, startDate, endDate).then((result) => {
      const issues = result.issues;
      const limitDateStart = new Date(startDate);
      const limitDateEnd = new Date(endDate + " 23:59:59");
      const allIssues = [];
      if (!issues) {
        reject(0);
        return;
      }
      fetchAllDataJiraIssues(
        jiraUrl,
        currentUser,
        issues,
        allIssues,
        limitDateStart,
        limitDateEnd
      ).then((worklogs) => {
        resolveWorklogs(worklogs);
      });
    });
  });
}

export function fetchAllJiraWorklogsDataForDateAndOwner(
  jiraUrl,
  currentUser,
  owner,
  startDate,
  endDate
) {
  return new Promise((resolveWorklogs, reject) => {
    fetchJiraWorklogsToDestiny(jiraUrl, owner, startDate, endDate).then((result) => {
      const issues = result.issues;
      const limitDateStart = new Date(startDate);
      const limitDateEnd = new Date(endDate + " 23:59:59");
      const allIssues = [];
      if (!issues) {
        reject(0);
        return;
      }
      fetchAllDataJiraIssues(
        jiraUrl,
        currentUser,
        issues,
        allIssues,
        limitDateStart,
        limitDateEnd
      ).then((worklogs) => {
        resolveWorklogs(worklogs);
      });
    });
  });
}


export function fetchAllDataJiraIssues(
  jiraUrl,
  currentUser,
  issues,
  allIssues,
  limitDateStart,
  limitDateEnd
) {
  let userData = [];
  issues.forEach((issue) => {
    let p = fetchAllWorklogsForIssue(jiraUrl, issue);
    allIssues.push(p);
    allIssues.push(
      new Promise((resolveAllData) =>
        p.then((worklogs) => {
          if (worklogs.length === 0) {
            userData.push({issue});
          } else {
            for (let workLog of worklogs) {
              let workLogDate = new Date(workLog.started);
              if (
                limitDateStart <= workLogDate &&
                limitDateEnd >= workLogDate &&
                (workLog.author.key === currentUser.name || workLog.author.name === currentUser.name)
              ) {
                workLog.issue = issue;
                userData.push(workLog);
              }
            }
          }
          resolveAllData(userData);
        })
      )
    );
  });
  return new Promise((resolve) => {
    Promise.all(allIssues).then(() => {
      resolve(userData);
    });
  });
}

export function fetchJiraDataOpenPetitions(jiraUrl, owner, project) {

  /*
  customfield_22631 // Sale estimate
  customfield_22634 // Internal estimate
  customfield_23215 // Expected start date
  customfield_23213 // Expected delivery date
  */
  return get(
      jiraUrl + '/rest/api/latest/search?jql=(reporter%20in%20(' +
      owner +
            ')%20or%20assignee%20in%20(' +
            owner +
            '))%20and%20project%20in%20(' +
            project +
            ')%20AND%20issuetype%20in%20standardIssueTypes()%20and%20status%20!=%20%27closed%27%20and%20status%20!=%20%27Delivered%27&fields=reporter,parent,summary,worklog,issuetype,status,aggregatetimeoriginalestimate,customfield_22631,customfield_22634,customfield_23215,customfield_23213&maxResults=50000'
    );
}


export function fetchSubtasks(jiraUrl, parentTask) {
  const apiUrl = jiraUrl + "/rest/api/latest/search?jql=parent=" + parentTask + "%20and%20assignee%20is%20EMPTY&fields=issuetype,created,updated,status,summary";
  return get(apiUrl);
}

export function fetchTasksAndSubtasks({ jiraUrl, keylist }) {
  const apiUrl = jiraUrl + `/rest/api/latest/search?jql=parent%20in%20(${keylist})%20or%20key%20in%20(${keylist})&fields=issuetype,created,updated,status,summary,worklog`;
  return get(apiUrl);
}
export function fetchVacations({ jiraUrl, workers, keylist, startDate, endDate }) {
  const apiUrl = jiraUrl + `/rest/api/latest/search?jql=parent%20in%20(${keylist})%20or%20key%20in%20(${keylist})%20and%20worklogAuthor%20in%20(${workers})%20and%20worklogDate%20%3E%3D%20%27${startDate}%27%20and%20worklogDate%20%3C%3D%20%27${endDate}%27&fields=summary,worklog`;
  return get(apiUrl);
}
export function groupWorklogsByIssue(
  worklogs
) {
  const issues = [...new Set(worklogs.map(worklog => {
    const issue = worklog.issue;
    issue.worklogs = issue.worklogs || [];
    issue.worklogs.push(worklog);
    return issue;
  }))];
  return issues;
  
}
