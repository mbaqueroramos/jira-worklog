"use strict";



  export function replaceClassItems(htmlCollection, oldClass, newClass) {
    htmlCollection.forEach((elem) => {
      elem.classList.remove(oldClass);
      elem.classList.toggle(newClass);
    });
  }

  export function toggleClasses(class1, class2) {
    const htmlCollection1 = Array.prototype.slice.call(
      document.getElementsByClassName(class1)
    );
    const htmlCollection2 = Array.prototype.slice.call(
      document.getElementsByClassName(class2)
    );
    replaceClassItems(htmlCollection1, class1, class2);
    replaceClassItems(htmlCollection2, class2, class1);
  }

  export function toggleToDateVisibility(evt) {
    toggleClasses("hide-to-date", "display-to-date");
  }

  export function findSiblingInput(elem) {
    return elem.parentElement.getElementsByTagName("input")[0];
  }

  export function hasClass(domItem, classValue) {
    const classes = domItem.className.split(" ");
    return classes.filter((currClass) => currClass === classValue).length;
  }

  export function removeClass(domItem, classValue) {
    if (!domItem) {
      return;
    }
    const classes = domItem.className.split(" ");
    domItem.className = classes
      .filter((currClass) => currClass !== classValue)
      .join(" ");
  }

  export function addClass(domItem, classValue) {
    if (!domItem) {
      return;
    }
    domItem.className = domItem.className + " " + classValue;
  }

  export function isVisible(elem) {
    if (!(elem instanceof Element))
      throw Error("DomUtil: elem is not an element.");
    const style = getComputedStyle(elem);
    if (style.display === "none") return false;
    if (style.visibility !== "visible") return false;
    if (style.opacity < 0.1) return false;
    if (
      elem.offsetWidth +
        elem.offsetHeight +
        elem.getBoundingClientRect().height +
        elem.getBoundingClientRect().width ===
      0
    ) {
      return false;
    }

    if (elem.parentElement === null) {
      return true;
    }
    return isVisible(elem.parentElement);
  }

  export function setupListener(id, event, handler) {
    try {
      document
        .getElementById(id)
        .addEventListener(event, handler);
    } catch (err) {
      console.error(err);
    }
    
  }

  export function stringifySafe(json) {
    const cache = [];
    return JSON.stringify(json, (_key, value) => {
      if (typeof value === 'object' && value !== null) {
        // Duplicate reference found, discard key
        if (cache.includes(value)) return;
    
        // Store value in our collection
        cache.push(value);
      }
      return value;
    });
  }

  export function removeChildren(htmlElement) {
    while (htmlElement.firstElementChild) {
      htmlElement.removeChild(htmlElement.firstElementChild);
    }
  }