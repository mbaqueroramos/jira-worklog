export function currentDay() {
  return yyyyMMdd(new Date());
}

export function yyyyMMdd(currentDate) {
  let mm = currentDate.getMonth() + 1;
  let dd = currentDate.getDate();
  let yyyy = currentDate.getFullYear();
  mm = (mm > 9 ? "" : "0") + mm;
  dd = (dd > 9 ? "" : "0") + dd;
  return yyyy + "-" + mm + "-" + dd;
}

export function firstDayOfMonthDate(d = new Date()) {
  return new Date(d.getFullYear(), d.getMonth(), 1);
}

export function firstDayOfMonthStr() {
  return yyyyMMdd(firstDayOfMonthDate());
}

export function lastDayOfMonthDate(d = new Date()) {
  return new Date(d.getFullYear(), d.getMonth() + 1, 0);
}

export function lastDayOfMonthStr() {
  return yyyyMMdd(lastDayOfMonthDate());
}

export function fourWeeksDayStr() {
  let d = new Date();
  d.setDate(d.getDate() - 28);
  return yyyyMMdd(d);
}

export function daysBetweenDates(date1, date2) {
  const difference = date2.getTime() - date1.getTime();
  return Math.ceil(difference / (1000 * 3600 * 24));
}

export function workdaysBetweenDates(strDate1, strDate2) {
  const fromDate = new Date(strDate1);
  const toDate = new Date(strDate2);
  const numDays = Math.abs(daysBetweenDates(fromDate, toDate)) + 1;
  let day = null;
  let workDays = 0;
  for (let i = 0; i < numDays; ++i) {
    day = addDaysToDate(strDate1, i);
    if (isWorkDay(day) || numDays === 1) {
      ++workDays;
    }
  }
  return workDays;
}

export function addDaysToDate(currDayStr, days) {
  const currentDate = new Date(Date.parse(currDayStr));
  currentDate.setDate(currentDate.getDate() + days);
  let mm = currentDate.getMonth() + 1;
  let dd = currentDate.getDate();
  let yyyy = currentDate.getFullYear();
  mm = (mm > 9 ? "" : "0") + mm;
  dd = (dd > 9 ? "" : "0") + dd;
  return yyyy + "-" + mm + "-" + dd;
}

export function convertToValidVariable(strDay) {
  return ('d' + strDay).replace(/-/g, 'd');
}

export function isWorkDay(currDayStr) {
  const currentDate = new Date(Date.parse(currDayStr));
  const dayOfWeek = currentDate.getDay();
  return dayOfWeek > 0 && dayOfWeek < 6;
}

export function timeOffset() {
  return new Date().getTimezoneOffset() / 0.6;
}

export function timeOffsetStr() {
  const currTimeOffset = timeOffset();
  let offsetStr = "+";
  if (currTimeOffset > 0) {
    offsetStr = "-";
    if (currTimeOffset < 1000) {
      offsetStr += "0";
    }
    offsetStr += currTimeOffset;
  } else {
    if (currTimeOffset > -1000) {
      offsetStr += "0";
    }
    offsetStr += -currTimeOffset;
  }

  return offsetStr;
}

export function shortenDateStr(str) {
  return str.substring(0,10) + ' ' + str.substring(11,16);
}

export function yyyyMMddStr(str) {
  return str.substring(0,10);
}