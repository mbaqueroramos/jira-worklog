
export function processIssues(leftIssues, rightIssues) {
    const fullProcess = findEquivalentIssues(leftIssues, rightIssues);
    findEquivalentWorklogsInIssues(fullProcess);
    return fullProcess;
}

export function findEquivalentIssues(leftIssues, rightIssues) {
    const fullProcess = [];
    for (const leftIssue of leftIssues) {
        const issueToTransfer = { ...leftIssue };
        issueToTransfer.version = issueToTransfer.fields?.versions?.map(version => version.name).pop();
        if (!issueToTransfer.version) {
            issueToTransfer.version = issueToTransfer.fields?.customfield_15820;
        }
        fullProcess.push(issueToTransfer);
        for (const rightIssue of rightIssues) {
            if (isEquivalentTask(issueToTransfer, rightIssue)) {
                issueToTransfer.equivalentIssue = rightIssue;
                break;
            }
        }
    }
    return fullProcess;
}

export function findEquivalentWorklogsInIssues(fullProcess) {
    
    for (const issue of fullProcess) {
        if (!issue.equivalentIssue) {
            continue;
        }
        issue.proccessedWorklogs = findEquivalentWorklogs(issue.worklogs, issue.equivalentIssue.worklogs);
    }
    return fullProcess;
}

export function findEquivalentWorklogs(leftWorklogs, rightWorklogs) {
    const proccessedWorklogs = [];
    for (const leftWorklog of leftWorklogs) {
        const worklogToTransfer = { ...leftWorklog };
        proccessedWorklogs.push(worklogToTransfer)
        for (const rightWorklog of rightWorklogs) {
            if (isEquivalentWorklog(worklogToTransfer, rightWorklog)) {
                worklogToTransfer.equivalentWorklog = rightWorklog;
                break;
            }
        }
    }
    return proccessedWorklogs;
}

function isEquivalentTask(worklogToTransfer, worklogDone) {
    return stringFormat(worklogDone.fields.summary).includes(worklogToTransfer.key);
}

function isEquivalentWorklog(worklogToTransfer, worklogDone) {
    return worklogDone.timeSpentSeconds === worklogToTransfer.timeSpentSeconds 
    && worklogDone.started === worklogToTransfer.started;
}

function stringFormat(value = '') {
    return value
        .replace(/[`~!@#$%^&*\='"<>\{\}\[\]\\\/]/gi, '')
        .toUpperCase();
}
